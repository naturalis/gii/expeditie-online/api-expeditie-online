from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse, request
from flask_jwt import JWT, jwt_required
from datetime import datetime, timedelta
from dateutil import parser as date_parser
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan
import logging, os, json

app = Flask(__name__)
app.config['SECRET_KEY'] = os.getenv('JWT_KEY')
app.config['JWT_EXPIRATION_DELTA'] = timedelta(days=3650)
app.config['PROPAGATE_EXCEPTIONS'] = True
prefix = "/api"
api = Api(app, prefix=prefix)

api_label = "naturalis expeditie online data api"
api_version = "v1.0"

ES_GENERAL_INDEX = None
ES_TAXON_INDEX = None
ES_NAMES_INDEX = None
ES_CONTROL_INDEX = None
ES_ACTIVE_INDEX = None

USERS = None
USERNAME_TABLE = None
USERID_TABLE = None

service_available = True
logger = None
available_languages = [ 'nl','en' ]
default_language = 'nl'
date_format = "%Y-%m-%dT%H:%M:%S"
default_date = "1970-01-01T00:00:00"

queries = {
    "general" : '{{ "query": {{ "bool": {{ "must": [ {{ "term": {{ "category": "{category}" }} }}, {{ "term": {{ "language": "{language}" }} }}, {{ "range": {{ "created": {{ "gte": "{timestamp}" }} }} }} ] }} }} }}',
    "all" : '{"query":{"match_all":{ }}}',
    "created" : '{{ "query": {{ "term" : {{ "language" : "{}" }} }}, "size": 1, "sort": [ {{ "created": {{ "order": "desc" }} }} ] }}',
    "search" : '{{"query": {{"bool" : {{"must": [ {{ "term": {{ "category": "{category}" }} }}, {{ "term": {{ "language": "{language}" }} }} ],"filter": {{ "wildcard": {{ "key": {{ "value": "*{search}*" }} }} }} }} }} }}',
    "search_names" : '{{"query": {{"bool" : {{ "filter": {{ "wildcard": {{ "scientific_name": {{ "value": "*{search}*" }} }} }} }} }} }}'
}

class User(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id

def initialize(app):
    initialize_logger()
    initialize_users()
    initialize_elasticsearch()

def initialize_logger(log_level=logging.INFO):
    global logger

    if os.getenv('DEBUGGING')=="1":
        log_level=logging.DEBUG

    logger=logging.getLogger("API")
    logger.setLevel(log_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    fh = logging.FileHandler(os.getenv('LOGFILE_PATH'))
    fh.setLevel(log_level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    
    if os.getenv('DEBUGGING')=="1":
        ch = logging.StreamHandler()
        ch.setLevel(log_level)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

def initialize_users():
    global USERS, USERNAME_TABLE, USERID_TABLE, logger

    for item in [ 'API_USER', 'API_PASS' ]:
        if (os.getenv(item)==None):
            logger.error("{} missing from ENV".format(item))
            set_service_available(False)

    if get_service_available() == True:
        USERS = [
            User(1, os.getenv('API_USER'), os.getenv('API_PASS')),
        ]
        USERNAME_TABLE = {u.username: u for u in USERS}
        USERID_TABLE = {u.id: u for u in USERS}

def initialize_elasticsearch():
    global logger, ES_GENERAL_INDEX, ES_TAXON_INDEX, ES_NAMES_INDEX, ES_CONTROL_INDEX
    try:
        for item in [ "ES_GENERAL_INDEX", "ES_TAXON_INDEX", "ES_NAMES_INDEX", "ES_CONTROL_INDEX", "ES_HOST", "ES_PORT", "LOGFILE_PATH" ]:
            if (os.getenv(item)==None):
                raise ValueError("'{}' not set in ENV".format(item))

                set_service_available(False)

        if get_service_available() == True:
            ES_GENERAL_INDEX = os.getenv('ES_GENERAL_INDEX')
            ES_TAXON_INDEX = os.getenv('ES_TAXON_INDEX')
            ES_NAMES_INDEX = os.getenv('ES_NAMES_INDEX')
            ES_CONTROL_INDEX = os.getenv('ES_CONTROL_INDEX')

    except Exception as e:
        logger.error("missing elasticsearch variables: {}".format(str(e)))
        set_service_available(False)

def get_elasticsearch_pulse():
    global logger, es
    try:
        es = Elasticsearch([{'host': os.getenv('ES_HOST'), 'port': os.getenv('ES_PORT')}])
        es.info()
        set_service_available(True)
    except Exception as e:
        logger.error("elasticsearch unreachable: {}".format(str(e)))
        set_service_available(False)

def set_service_available(state):
   global service_available
   service_available = state

def get_service_available():
   global service_available
   return service_available

def get_language():
    """ Returns the current language. If not retrievable from the command line arguments,
    the default language will be returned

    Returns:
        string: of language to be used
    """
    global available_languages, default_language
    language = default_language

    try:
        args = parser.parse_args()
        if hassattr(args, 'language'):
            language = args['language']
            if not language in available_languages:
                raise ValueError("unknown language '{}'".format(language))
    except:
        print('Argument parser failed, using default language')

    return language

def get_date():
    """ Returns the date. If not retrievable from the command line arguments,
    the default date will be returned

    Returns:
        string: of date
    """
    global date_format, default_date
    date = default_date
    
    try:
        args = parser.parse_args()
        if hassattr(args, 'date'):
            date = args['date']
    except:
        print('Argument parser failed, using default date')

    try:
        formatted_date = datetime.strptime(date,date_format)
    except Exception as e:
        raise ValueError("invalid datetime format; use: '{}'".format(date_format))

    return formatted_date.strftime(date_format)

def get_search():
    """ Returns the search boolean. If not retrievable from the command line arguments,
    the default search value will be returned (False)

    Returns:
        boolean || string: whether to search, so yes, what for
    """
    search = False
    try:
        args = parser.parse_args()
        if hassattr(args, 'search'):
            search = args['search']
            search = search.replace('"','\\"')
    except:
        print('Argument parser failed, using default search')

    return search

def get_limit():
    """ Returns the limit. If not retrievable from the command line arguments,
    the default limit value will be returned (False)

    Returns:
        boolean: whether to limit, so yes, how much 
    """
    limit = False
    try:
        args = parser.parse_args()
        if hassattr(args, 'limit'):
            try:
                limit = int(limit)
            except Exception as e:
                limit = False
    except:
        print('Argument parser failed, using default limit')

    return limit

def get_result_size(query):
    global es,ES_ACTIVE_INDEX,ES_GENERAL_INDEX,ES_TAXON_INDEX,ES_NAMES_INDEX

    # getting count for indices exceeding 10,000 doc mark
    if ES_ACTIVE_INDEX in [ ES_TAXON_INDEX, ES_NAMES_INDEX]:
        es.indices.refresh(ES_ACTIVE_INDEX)
        count = es.cat.count(ES_ACTIVE_INDEX,params={"format":"json"})
        return int(count.pop()["count"])
    else:
        response = run_elastic_query(query,size=1)
        return response["hits"]["total"]["value"]

def get_general_items(language,category,timestamp,search,limit):
    global queries

    if search == False:
        query = queries["general"].format(category=category,language=language,timestamp=timestamp)
    else:
        query = queries["search"].format(category=category,language=language,search=search)

    items = []
    for hit in scan(es, index=ES_ACTIVE_INDEX, query=json.loads(query)):
        items.append(hit["_source"])
        if not limit == False and len(items) >= limit:
            break

    return { "size" : get_result_size(query), "items" : items }

class RootRequest(Resource):
    def get(self):
        global api_label, api_version
        return { api_label : api_version }

class GetDocumentation(Resource):
    @jwt_required()
    def get(self):
        global api_label, api_version, prefix, available_languages, default_language, date_format, default_date

        services = []
        services.append({"description" : "taxa", "endpoint" : prefix + "/taxon"})
        services.append({"description" : "highlights", "endpoint" : prefix + "/highlight"})
        services.append({"description" : "preparation types", "endpoint" : prefix + "/preparation-type"})
        services.append({"description" : "type statuses", "endpoint" : prefix + "/type-status"})
        services.append({"description" : "subcollections", "endpoint" : prefix + "/subcollection"})
        services.append({"description" : "collectors", "endpoint" : prefix + "/collector"})
        services.append({"description" : "special collections", "endpoint" : prefix + "/special-collection"})
        services.append({"description" : "names", "endpoint" : prefix + "/names"})
        services.append({"description" : "last updated", "endpoint" : prefix + "/last-updated"})
        services.append({"description" : "doc (this)", "endpoint" : prefix + "/doc"})

        parameters = []
        parameters.append({
            "parameter" : "language",
            "description" : "language; restricts results to specific language",
            "values" : available_languages,
            "mandatory" : "no (defaults to '{}')".format(default_language),
            "example" : "/api/taxon?language=nl"})

        parameters.append({
            "parameter" : "date",
            "description" : "datetime; serves documents created on or after specified datetime",
            "format" : date_format,
            "mandatory" : "no (default: '{}')".format(default_date),
            "example" : "/api/taxon?date=2021-06-01T12:00:00"})

        return {
            "api" : { api_label : api_version },
            "services" : services,
            "parameters" : parameters}

class GetLastUpdated(Resource):
    @jwt_required()
    def get(self):
        global es, queries, date_format, ES_GENERAL_INDEX, ES_TAXON_INDEX, ES_NAMES_INDEX, ES_ACTIVE_INDEX
        try:
            language = get_language()
            query = queries["created"].format(language)
            created_datestrings = []

            for index in [ES_GENERAL_INDEX, ES_TAXON_INDEX, ES_NAMES_INDEX]:

                if not es.indices.exists(index=index):
                    continue

                ES_ACTIVE_INDEX = index
                response = run_elastic_query(query,size=1,_source_includes="created")

                try:
                    created_datestrings.append(response["hits"]["hits"][0]["_source"]["created"])
                except Exception as e:
                    pass

            if len(created_datestrings) > 0:
                d = date_parser.parse(max(created_datestrings))
                r = d.strftime(date_format)
            else:
                r = None

            log_usage(language=language,category="last-updated",hits="1")

            return { "last_updated" : r }
        except Exception as e:
            log_request_error(str(e))
            return { "error": str(e) }

class GetTaxon(Resource):
    @jwt_required()
    def get(self):
        global ES_TAXON_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_TAXON_INDEX
            category = 'taxon'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)

            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),search=search,limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetPreparationType(Resource):
    @jwt_required()
    def get(self):
        global ES_GENERAL_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_GENERAL_INDEX
            category = 'preparation_type'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)
            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetTypeStatus(Resource):
    @jwt_required()
    def get(self):
        global ES_GENERAL_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_GENERAL_INDEX
            category = 'type_status'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)
            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetSubcollection(Resource):
    @jwt_required()
    def get(self):
        global ES_GENERAL_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_GENERAL_INDEX
            category = 'subcollection'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)
            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetHighlight(Resource):
    @jwt_required()
    def get(self):
        global ES_GENERAL_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_GENERAL_INDEX
            category = 'highlight'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)
            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetCollector(Resource):
    @jwt_required()
    def get(self):
        global ES_GENERAL_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_GENERAL_INDEX
            category = 'collector'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)
            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetSpecialCollection(Resource):
    @jwt_required()
    def get(self):
        global ES_GENERAL_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_GENERAL_INDEX
            category = 'special_collection'
            language = get_language()
            timestamp = get_date()
            search = get_search()
            limit = get_limit()
            reduced = get_general_items(language,category,timestamp,search,limit)
            log_usage(language=language,category=category,timestamp=timestamp,hits=len(reduced["items"]),limit=limit)
            return reduced
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

class GetNames(Resource):
    @jwt_required()
    def get(self):
        global es,queries,ES_NAMES_INDEX, ES_ACTIVE_INDEX
        try:
            ES_ACTIVE_INDEX = ES_NAMES_INDEX
            search = get_search()

            if search == False:
                query = queries["all"]
            else:
                query = queries["search_names"].format(search=search)

            items = []
            limit = get_limit()
            for hit in scan(es, index=ES_ACTIVE_INDEX, query=json.loads(query)):
                items.append(hit["_source"])
                if not limit == False and len(items) >= limit:
                    break

            size = get_result_size(query)

            log_usage(category="names",hits=size,search=search,limit=limit)

            return { "size" : size, "items" : items }
        except Exception as e:
            log_request_error(str(e))
            return {"error": str(e) }

def run_elastic_query(query,**kwargs):
    global es, ES_ACTIVE_INDEX
    logger.debug(ES_ACTIVE_INDEX)
    logger.debug(query)
    return es.search(index=ES_ACTIVE_INDEX,body=query,request_timeout=5,**kwargs)

def get_documents_status():
    global es, ES_CONTROL_INDEX
    response = es.search(index=ES_CONTROL_INDEX,body='{}')
    try:
        documents_status=response["hits"]["hits"][0]["_source"]["status"]
    except Exception as e:
        log_request_error("document status unavailable (defaulting to 'ready')")
        documents_status="ready"
    return documents_status

def log_usage(language="",category="",timestamp="",hits="",search="",limit=False):
    global logger
    endpoint=request.path
    remote_addr=request.remote_addr

    if not request.headers.getlist("X-Forwarded-For"):
       remote_addr = request.remote_addr
    else:
       remote_addr = request.headers.getlist("X-Forwarded-For")[0]

    logger.info("{remote_addr} - {endpoint} - {params} - {hits}"
        .format(
            remote_addr=remote_addr,
            endpoint=endpoint,
            params=json.dumps({
                "language":language,
                "category":category,
                "timestamp":timestamp,
                "search":search,
                "limit":limit
                }),
            hits=hits))

def log_request_error(error="unknown error"):
    global logger
    endpoint=request.path
    remote_addr=request.remote_addr
    logger.error("{remote_addr} - {endpoint} - {error}".format(remote_addr=remote_addr,endpoint=endpoint,error=error))

@app.before_request
def PreRequestHandler():
    get_elasticsearch_pulse()

    if not get_documents_status() == "ready":
        return jsonify({ "error": "document store busy" })

    if get_service_available() == False:
        log_request_error("service unavailable")
        return jsonify({ "error": "service unavailable" })

@app.errorhandler(404)
def page_not_found(e):
    return jsonify({ "error" : e.description }), 404

# https://pythonhosted.org/Flask-JWT/

def authenticate(username, password):
    global USERNAME_TABLE

    user = USERNAME_TABLE.get(username, None)

    if user is None:
        logger.info("authentication failure - username: '{}'; password: '{}'".format(username, password))

    if user and user.password.encode('utf-8') == password.encode('utf-8'):
        return user

def identity(payload):
    global USERID_TABLE
    user_id = payload['identity']
    return USERID_TABLE.get(user_id, None)

jwt = JWT(app, authenticate, identity)

@jwt.jwt_error_handler
def customized_error_handler(e):
    return jsonify({ "error" : e.error }), e.status_code

parser = reqparse.RequestParser()
parser.add_argument('language')
parser.add_argument('date')
parser.add_argument('search')
parser.add_argument('limit')

api.add_resource(RootRequest, '/')
api.add_resource(GetTaxon, '/taxon')
api.add_resource(GetHighlight, '/highlight')
api.add_resource(GetPreparationType, '/preparation-type')
api.add_resource(GetTypeStatus, '/type-status')
api.add_resource(GetSubcollection, '/subcollection')
api.add_resource(GetCollector, '/collector')
api.add_resource(GetSpecialCollection, '/special-collection')
api.add_resource(GetNames, '/names')
api.add_resource(GetLastUpdated, '/last-updated')
api.add_resource(GetDocumentation, '/doc')

initialize(app)

if __name__ == '__main__':
    app.run(debug=(os.getenv('FLASK_DEBUG')=="1"),host='0.0.0.0')
